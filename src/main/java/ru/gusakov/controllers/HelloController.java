package ru.gusakov.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Controller
public class HelloController {
    @GetMapping("/hello/{user}")
    public String hello(@PathVariable(name = "user") Optional<String> name, Model model) {
        if (name.isPresent()){
            model.addAttribute("greeting", name.get());
        }
        else model.addAttribute("greeting", "user");
        return "index";
    }
}
