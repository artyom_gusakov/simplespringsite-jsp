package ru.gusakov.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;


public class WebAppInitializer implements WebApplicationInitializer {

    public void onStartup(ServletContext servletContext) throws ServletException {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!START UP SPRING DISPATCHER SERVLET");

        //Регистрируем класс настроек бинов
        //В нем есть @ComponentScan позволяет автоматом найти классы аннотированные спринговыми аннотациями
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(ApplicationContextConfig.class);

        /*Настраиваем спринговый сервлет (DispatcherServlet)*/
        //1. Регистрируем спринговый сервлет servletContext.addServlet
        //2. В спринговый сервлет передаем контекст context - в нем указаны все наши бины
        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("Spring Dispatcher", new DispatcherServlet(context));
        /*Устанавливает приоритет loadOnStartup на сервлете, представленном этой динамической службой ServletRegistration.
          Значение loadOnStartup, большее или равное нулю, указывает контейнеру приоритет инициализации сервлета.
          В этом случае контейнер должен создать экземпляр и инициализировать сервлет во время фазы инициализации ServletContext,
          то есть после того, как он вызвал все объекты ServletContextListener, настроенные для ServletContext, в их методе ServletContextListener # contextInitialized
          Если loadOnStartup является отрицательным целым числом, контейнер может создавать экземпляр и инициализировать сервлет лениво.
          Значение по умолчанию для loadOnStartup равно -1.Вызов этого метода отменяет любые предыдущие настройки.*/
        dispatcherServlet.setLoadOnStartup(1);

        //Регистрируем запросы по какому url будет обрабатывать наш сервлет
        dispatcherServlet.addMapping("/");

        //Добавление слушателя загрузки контекста
        //Выполняет загрузку сприногового контестка в сервлет???
//        servletContext.addListener(new ContextLoaderListener(context));

        //добавляем преобразование символов в utf-8
        // UTF8 Character Filter.
        FilterRegistration.Dynamic fr = servletContext.addFilter("encodingFilter", CharacterEncodingFilter.class);

        fr.setInitParameter("encoding", "UTF-8");
        fr.setInitParameter("forceEncoding", "true");
        fr.addMappingForUrlPatterns(null, true, "/*");
    }
}
